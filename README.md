# OpenML dataset: iris_reproduced

https://www.openml.org/d/44154

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: R.A. Fisher  

**Source**: [UCI](https://archive.ics.uci.edu/ml/datasets/Iris) - 1936 - Donated by Michael Marshall  

**Please cite**:   




**Iris Plants Database**  

This is perhaps the best known database to be found in the pattern recognition literature.  Fisher's paper is a classic in the field and is referenced frequently to this day.  (See Duda & Hart, for example.)  The data set contains 3 classes of 50 instances each, where each class refers to a type of iris plant.  One class is     linearly separable from the other 2; the latter are NOT linearly separable from each other.



Predicted attribute: class of iris plant.  

This is an exceedingly simple domain.  

 

### Attribute Information:

    1. sepal length in cm

    2. sepal width in cm

    3. petal length in cm

    4. petal width in cm

    5. class: 

       -- Iris Setosa

       -- Iris Versicolour

       -- Iris Virginica

 From OpenML: https://www.openml.org/d/61

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44154) of an [OpenML dataset](https://www.openml.org/d/44154). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44154/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44154/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44154/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

